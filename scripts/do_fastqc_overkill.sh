#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=30:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n


module add mugqic/java/openjdk-jdk1.8.0_72 mugqic/fastqc/0.11.2

cd /home/efournie/MS_sRNA/

pushd output/Human/pipeline/
for i in cutadapt/*.overkill.fastq.gz
do
  SAMPLENAME=`basename $i .fastq.gz`
  FASTQFILE=$i
  
  mkdir -p fastqc/$SAMPLENAME.cutadapt
 
  fastqc -o fastqc/$SAMPLENAME.cutadapt $i
done
popd

pushd output/Mouse/pipeline/
for i in cutadapt/*.overkill.fastq.gz
do
  SAMPLENAME=`basename $i .fastq.gz`
  FASTQFILE=$i
  
  mkdir -p fastqc/$SAMPLENAME.cutadapt
 
  fastqc -o fastqc/$SAMPLENAME.cutadapt $i 
done
popd