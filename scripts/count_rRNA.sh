#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=30:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n

module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11

cd /home/efournie/MS_sRNA/

#pushd output/Human/pipeline/
#for i in trim/*
#do
#  SAMPLENAME=`basename $i fastq.gz`
#  FASTQFILE=$i/*.fastq.gz
#  
#  mkdir -p rRNA/$SAMPLENAME
# 
#  gunzip -c $FASTQFILE | 
#  bwa mem  \
#    -M -t 10 \
#    $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.hg19/annotations/rrna_bwa_index/Homo_sapiens.hg19.UCSC2009-03-08.rrna.fa \
#    /dev/stdin | \
#  java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
#    VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
#    TMP_DIR=$LSCRATCH \
#    INPUT=/dev/stdin \
#    OUTPUT=rRNA/$SAMPLENAME.bam \
#    SORT_ORDER=coordinate \
#    MAX_RECORDS_IN_RAM=1750000 && \
#  python $PYTHON_TOOLS/rrnaBAMcounter.py \
#    -i rRNA/$SAMPLENAME.bam \
#    -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.hg19/annotations/Homo_sapiens.hg19.UCSC2009-03-08.gtf \
#    -o rRNA/$SAMPLENAME.stats.tsv \
#    -t transcript
#done
#popd

pushd output/Mouse/pipeline/
for i in trim/*
do
  SAMPLENAME=`basename $i`
  FASTQFILE=$i/*.fastq.gz
  
  mkdir -p rRNA/$SAMPLENAME
 
  gunzip -c $FASTQFILE | 
  bwa mem  \
    -M -t 1 \
    $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.GRCm38/annotations/rrna_bwa_index/Mus_musculus.GRCm38.Ensembl83.rrna.fa \
    /dev/stdin | \
  java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
    VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
    TMP_DIR=$LSCRATCH \
    INPUT=/dev/stdin \
    OUTPUT=rRNA/$SAMPLENAME.bam \
    SORT_ORDER=coordinate \
    MAX_RECORDS_IN_RAM=1750000 && \
  python $PYTHON_TOOLS/rrnaBAMcounter.py \
    -i rRNA/$SAMPLENAME.bam \
    -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.GRCm38/annotations/Mus_musculus.GRCm38.Ensembl83.gtf \
    -o rRNA/$SAMPLENAME.stats.tsv \
    -t transcript
done
popd