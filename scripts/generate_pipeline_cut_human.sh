rnaseq.py -l debug -s '1-22' \
    -o MS_sRNA/output/Human/pipeline_cut \
    -r input/Human/cutreads/readset.txt \
    -d input/Human/design.txt \
    -c $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.base.ini \
       $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/Homo_sapiens.GRCh38.ini \
       $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
       input/override.star.ini \
       input/override.human.ini