rnaseq.py -l debug -s 1-22 \
    -o MS_sRNA/output/Mouse/pipeline_cut \
    -r input/Mouse/cutreads/readset.txt \
    -d input/Mouse/design.txt \
    -c $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.base.ini \
       $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.GRCm38/Mus_musculus.GRCm38.ini \
       $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
       input/override.star.ini