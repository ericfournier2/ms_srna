#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=30:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n


module add mugqic/java/openjdk-jdk1.8.0_72 mugqic/fastqc/0.11.2

#cd /home/efournie/MS_sRNA/
#
#for i in input/Human/*.fastq.gz
#do
#  SAMPLENAME=`basename $i .fastq.gz`
#  FASTQFILE=$i
#  
#  mkdir -p output/Human/pipeline/cutadapt
#  mkdir -p output/Human/pipeline/jobs  
#   
#    cat > output/Human/pipeline/jobs/$SAMPLENAME.cutadapt.sh <<EOF
##!/bin/bash
##PBS -A fhq-091-aa
##PBS -l walltime=2:00:00
##PBS -l nodes=1:ppn=1
##PBS -q qwork
##PBS -r n    
##PBS -o output/Human/pipeline/jobs/$SAMPLENAME.cutadapt.sh.stdout
##PBS -e output/Human/pipeline/jobs/$SAMPLENAME.cutadapt.sh.stderr
#
#cd /home/efournie/MS_sRNA/   
#
#/home/efournie/.local/bin/cutadapt -f fastq \
#-b GCCTTGGCACCCGAGAATTCCA \
#-b CGACAGGTTCAGAGTTCTACAGTCCGACGATC \
#-b TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC \
#$i 2> output/Human/pipeline/cutadapt/$SAMPLENAME.1.log |
#/home/efournie/.local/bin/cutadapt -f fastq \
#-b GCCTTGGCACCCGAGAATTCCA \
#-b CGACAGGTTCAGAGTTCTACAGTCCGACGATC \
#-b TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC \
#/dev/stdin 2> output/Human/pipeline/cutadapt/$SAMPLENAME.2.log|
#/home/efournie/.local/bin/cutadapt -f fastq \
#-b GCCTTGGCACCCGAGAATTCCA \
#-b CGACAGGTTCAGAGTTCTACAGTCCGACGATC \
#-b TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC \
#/dev/stdin 2> output/Human/pipeline/cutadapt/$SAMPLENAME.3.log |    gzip -c > output/Human/pipeline/cutadapt/$SAMPLENAME.overkill.fastq.gz
#EOF
#
#    qsub output/Human/pipeline/jobs/$SAMPLENAME.cutadapt.sh 
#    
#done

for i in input/Mouse/*.fastq.gz
do
  SAMPLENAME=`basename $i .fastq.gz`
  FASTQFILE=$i
  
  mkdir -p output/Mouse/pipeline/cutadapt
  mkdir -p output/Mouse/pipeline/jobs
 
    cat > output/Mouse/pipeline/jobs/$SAMPLENAME.cutadapt.sh <<EOF
#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=2:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n    
#PBS -o output/Mouse/pipeline/jobs/$SAMPLENAME.cutadapt.sh.stdout
#PBS -e output/Mouse/pipeline/jobs/$SAMPLENAME.cutadapt.sh.stderr

cd /home/efournie/MS_sRNA/   

/home/efournie/.local/bin/cutadapt -f fastq \
-b GCCTTGGCACCCGAGAATTCCA \
-b CGACAGGTTCAGAGTTCTACAGTCCGACGATC \
-b TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC \
$i 2> output/Mouse/pipeline/cutadapt/$SAMPLENAME.1.log |
/home/efournie/.local/bin/cutadapt -f fastq \
-b GCCTTGGCACCCGAGAATTCCA \
-b CGACAGGTTCAGAGTTCTACAGTCCGACGATC \
-b TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC \
/dev/stdin 2> output/Mouse/pipeline/cutadapt/$SAMPLENAME.2.log |
/home/efournie/.local/bin/cutadapt -f fastq \
-b GCCTTGGCACCCGAGAATTCCA \
-b CGACAGGTTCAGAGTTCTACAGTCCGACGATC \
-b TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC \
/dev/stdin 2> output/Mouse/pipeline/cutadapt/$SAMPLENAME.3.log |  gzip -c > output/Mouse/pipeline/cutadapt/$SAMPLENAME.overkill.fastq.gz
EOF

    qsub output/Mouse/pipeline/jobs/$SAMPLENAME.cutadapt.sh 
done
