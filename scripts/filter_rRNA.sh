#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=30:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n

module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11

cd /home/efournie/MS_sRNA/

pushd output/Human/pipeline/
for i in cutadapt/*.fastq.gz
do
  SAMPLENAME=`basename $i .fastq.gz`
  FASTQFILE=$i
  
  mkdir -p rRNA/$SAMPLENAME
 
    cat > jobs/$SAMPLENAME.filter_rRNA.sh <<EOF
#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=2:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n    
#PBS -o jobs/$SAMPLENAME.filter_rRNA.sh.stdout
#PBS -e jobs/$SAMPLENAME.filter_rRNA.sh.stderr

cd /home/efournie/MS_sRNA/output/Human/pipeline/   

module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 mugqic/samtools/1.3

#  gunzip -c $FASTQFILE | 
#  bwa mem  \
#    -M -t 10 \
#    $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.hg19/annotations/rrna_bwa_index/Homo_sapiens.hg19.UCSC2009-03-08.rrna.fa \
#    /dev/stdin | \
#  java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
#    VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
#    TMP_DIR=$LSCRATCH \
#    INPUT=/dev/stdin \
#    OUTPUT=rRNA/$SAMPLENAME.bam \
#    SORT_ORDER=coordinate \
#    MAX_RECORDS_IN_RAM=1750000
    
  mkdir -p no_rRNA
  samtools view -f 4 rRNA/$SAMPLENAME.bam |
  java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx30G -jar $PICARD_HOME/SamToFastq.jar \
    VALIDATION_STRINGENCY=LENIENT \
    INPUT=/dev/stdin \
    FASTQ=/dev/stdout |
    gzip > no_rRNA/$SAMPLENAME.fastq.gz

    
#  python $PYTHON_TOOLS/rrnaBAMcounter.py \
#    -i rRNA/$SAMPLENAME.bam \
#    -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.hg19/annotations/Homo_sapiens.hg19.UCSC2009-03-08.gtf \
#    -o rRNA/$SAMPLENAME.stats.tsv \
#    -t transcript
EOF

    qsub jobs/$SAMPLENAME.filter_rRNA.sh  
done
popd

pushd output/Mouse/pipeline/
for i in cutadapt/*.fastq.gz
do
  SAMPLENAME=`basename $i .fastq.gz`
  FASTQFILE=$i
  
  mkdir -p rRNA/$SAMPLENAME
 
    cat > jobs/$SAMPLENAME.filter_rRNA.sh <<EOF
#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=2:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n    
#PBS -o jobs/$SAMPLENAME.filter_rRNA.sh.stdout
#PBS -e jobs/$SAMPLENAME.filter_rRNA.sh.stderr

cd /home/efournie/MS_sRNA/output/Mouse/pipeline/   

module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/bvatools/1.6 mugqic/bwa/0.7.12 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.11 mugqic/samtools/1.3

#  gunzip -c $FASTQFILE | 
#  bwa mem  \
#    -M -t 10 \
#    $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.GRCm38/annotations/rrna_bwa_index/Mus_musculus.GRCm38.Ensembl83.rrna.fa \
#    /dev/stdin | \
#  java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
#    VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
#    TMP_DIR=$LSCRATCH \
#    INPUT=/dev/stdin \
#    OUTPUT=rRNA/$SAMPLENAME.bam \
#    SORT_ORDER=coordinate \
#    MAX_RECORDS_IN_RAM=1750000
    
  mkdir -p no_rRNA
  samtools view -f 4 rRNA/$SAMPLENAME.bam |
  java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=4194304 -Xmx30G -jar $PICARD_HOME/SamToFastq.jar \
    VALIDATION_STRINGENCY=LENIENT \
    INPUT=/dev/stdin \
    FASTQ=/dev/stdout |
    gzip > no_rRNA/$SAMPLENAME.fastq.gz

    
#  python $PYTHON_TOOLS/rrnaBAMcounter.py \
#    -i rRNA/$SAMPLENAME.bam \
#    -g $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.GRCm38/annotations/Mus_musculus.GRCm38.Ensembl83.gtf \
#    -o rRNA/$SAMPLENAME.stats.tsv \
#    -t transcript
EOF

    qsub jobs/$SAMPLENAME.filter_rRNA.sh  
done
popd