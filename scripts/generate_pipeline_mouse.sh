rnaseq.py -l debug -s 1-15 \
    -o $PARALLEL_SCRATCH_MP2_WIPE_ON_AUGUST_2016/MS_sRNA/output/Mouse/pipeline \
    -r input/Mouse/readset.txt \
    -d input/Mouse/design.txt \
    -c $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.base.ini \
       $MUGQIC_INSTALL_HOME/genomes/species/Mus_musculus.GRCm38/Mus_musculus.GRCm38.ini \
       $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
       input/override.star.ini