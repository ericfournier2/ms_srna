cd /home/efournie/MS_sRNA

# Generate a version of the fai file with chr prefix
#for bam in input/Human/*.bam
#do
#    sampleName=`basename $bam .bam`    
#
#    cat > $sampleName.sh <<EOF
##!/bin/bash
##PBS -l nodes=1:ppn=1
##PBS -l walltime=4:00:00
##PBS -A fhh-112-aa
##PBS -o $sampleName.sh.stdout
##PBS -e $sampleName.sh.stderr
##PBS -V
##PBS -N $sampleName.sh
#
#module load mugqic/samtools/0.1.19 mugqic/bedtools/2.22.1 mugqic/ucsc/20140212
#
#cd /home/efournie/MS_sRNA
#
#mkdir -p tracks/$sampleName tracks/bigWig
#
#nmblines=\$(samtools view -F 256 $bam | wc -l)
#scalefactor=0\$(echo "scale=2; 1 / (\$nmblines / 10000000);" | bc)
#genomeCoverageBed -bg -split -scale \$scalefactor -ibam $bam -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/Homo_sapiens.hg19.fa.fai > tracks/$sampleName/$sampleName.bedGraph
#sed -e 's/^\([1-9MXY]\)/chr\1/' tracks/$sampleName/$sampleName.bedGraph | sed -e 's/^chrMT/chrM/' | grep -e 'chr' > tracks/$sampleName/$sampleName.ucsc.bedGraph
#
#
#sort -k1,1 -k2,2n tracks/$sampleName/$sampleName.ucsc.bedGraph > tracks/$sampleName/$sampleName.bedGraph.ucsc.sorted
#bedGraphToBigWig tracks/$sampleName/$sampleName.bedGraph.ucsc.sorted  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/Homo_sapiens.hg19.fa.fai  tracks/bigWig/$sampleName.bw
#EOF
#
#    qsub $sampleName.sh    
#done

for bam in input/Mouse/*.bam
do
    sampleName=`basename $bam .bam`    

    cat > $sampleName.sh <<EOF
#!/bin/bash
#PBS -l nodes=1:ppn=1
#PBS -l walltime=4:00:00
#PBS -A fhh-112-aa
#PBS -o $sampleName.sh.stdout
#PBS -e $sampleName.sh.stderr
#PBS -V
#PBS -N $sampleName.sh

module load mugqic/samtools/0.1.19 mugqic/bedtools/2.22.1 mugqic/ucsc/20140212

cd /home/efournie/MS_sRNA

mkdir -p tracks/$sampleName tracks/bigWig

nmblines=\$(samtools view -F 256 $bam | wc -l)
scalefactor=0\$(echo "scale=2; 1 / (\$nmblines / 10000000);" | bc)
genomeCoverageBed -bg -split -scale \$scalefactor -ibam $bam -g /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai > tracks/$sampleName/$sampleName.bedGraph
sed -e 's/^\([1-9MXY]\)/chr\1/' tracks/$sampleName/$sampleName.bedGraph | sed -e 's/^chrMT/chrM/' | grep -e 'chr' > tracks/$sampleName/$sampleName.ucsc.bedGraph


sort -k1,1 -k2,2n tracks/$sampleName/$sampleName.ucsc.bedGraph > tracks/$sampleName/$sampleName.bedGraph.ucsc.sorted
bedGraphToBigWig tracks/$sampleName/$sampleName.bedGraph.ucsc.sorted  /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.fa.fai  tracks/bigWig/$sampleName.bw
EOF

    qsub $sampleName.sh    
done
