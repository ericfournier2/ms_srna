# Create symbolic links for the aggressively cut sequences
for species in Human Mouse
do
    mkdir -p input/$species/cutreads
    pushd input/$species/cutreads
    ln -s ../../../output/$species/pipeline/cutadapt/*.single.overkill.fastq.gz ./
    popd
done

#### Create new readset files, put them in the cutreads folders ###