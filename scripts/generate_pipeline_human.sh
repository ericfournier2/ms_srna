rnaseq.py -l debug -s '1-22' \
    -o $PARALLEL_SCRATCH_MP2_WIPE_ON_AUGUST_2016/MS_sRNA/output/Human/pipeline \
    -r input/Human/readset.txt \
    -d input/Human/design.txt \
    -c $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.base.ini \
       $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.hg19/Homo_sapiens.hg19.ini \
       $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.mammouth.ini \
       input/override.star.ini