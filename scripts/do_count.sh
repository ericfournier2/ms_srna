#!/bin/bash
#PBS -A fhq-091-aa
#PBS -l walltime=30:00:00
#PBS -l nodes=1:ppn=1
#PBS -q qwork
#PBS -r n

module load mugqic/python/2.7.11
module load mugqic/mugqic_pipelines/2.2.0

cd /home/efournie/MS_sRNA

for bamfile in input/Mouse/*.bam
do
  bn=`basename $bamfile .bam`
  htseq-count -f bam $bamfile /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.GRCm38/annotations/Mus_musculus.GRCm38.Ensembl83.gtf > output/Mouse/$bn.count
done
paste output/Mouse/*.count > output/Mouse/All.count


for bamfile in input/Human/*.bam
do
  htseq-count -f bam $bamfile input/Human/Homo_sapiens.GRCh37.75.gtf > output/Human/$bn.count
done
paste output/Human/*.count > output/Human/All.count